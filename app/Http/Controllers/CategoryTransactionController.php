<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryTransactionController extends ApiController {
    public function index(Category $caterory) {
        $transactions = $caterory->products()->with('transactions')->get()->pluck('transactions')->flatten();
        return $this->showAll($transactions);
    }
}
