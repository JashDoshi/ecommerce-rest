<?php

namespace App\Http\Controllers;

use App\Traits\ResponseHelper;
use Illuminate\Http\Request;
use Illuminate\Http\ResponseTrait;

class ApiController extends Controller {
    use ResponseHelper;
}
