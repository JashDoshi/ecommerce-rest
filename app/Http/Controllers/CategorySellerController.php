<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategorySellerController extends ApiController {
    public function index(Category $caterory) {
        $sellers = $caterory->products()->with('seller')->get()->pluck('seller')->unique()->values();
        return $this->showAll($sellers);
    }
}
